import React from 'react';

import Styles from '../../../views/Home/Home.module.css';
import SquadMember from '../SquadDetail/SquadMember/SquadMember';
import moment from 'moment';

class SquadDetail extends React.Component {

    state = {
        gameId: this.props.gameId,
        squadId: this.props.squadId,
        squad: null,
        accessToken: sessionStorage.getItem("accessToken"),
        lng: null,
        lat: null,
        currTime: "",
        endTime: "",
    }

    componentDidMount = async () => {


        try {
            const resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.gameId}/squad/${this.state.squadId}`, {
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken
                }
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });
            console.log(resp);
            if (resp[0] === 200) {
                this.setState({ squad: resp[1] })
            }

        } catch (e) {
            console.log(e);
        }
    }

    handleLeave = async () => {
        try {
            const resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.gameId}/squad/${this.state.squadId}/leave`, {
                method: "DELETE",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken
                }
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });
            console.log(resp);
            if (resp[0] === 200) {
                this.props.updateSquadID(0);
            }

        } catch (e) {
            console.log(e);
        }
    }

    getGeoLocation = () => {
        let cords = {};
        let allowed = null;
        if(navigator.geolocation) {
            allowed = true;
            cords = navigator.geolocation.getCurrentPosition(this.printLocationInfo);
        } else {
            console.log("no");
        }
        console.log(cords)
        return allowed;
    }

    printLocationInfo = (position) => {
        const lng = position.coords.longitude;
        const lat = position.coords.latitude;
        console.log("lng " + lng + " lat " + lat);
        this.setState({
            lng: lng,
            lat: lat,
        })

        const currTime = moment().format("h:mm:ss")
        const endTime = moment().add(10, "m").format("h:mm:ss");

        console.log(currTime);
        console.log(endTime);
        this.checkInFetch(currTime, endTime);
    }

    handleCheckIn = () => {
        //console.log("lng " + this.state.lng + " lat " + this.state.lat);
        this.getGeoLocation();
    }

    getSquadMemberId = () => {
        let squadMemberId = 0;

        this.state.squad.squadMemberList.forEach(member => {
            if(member.squadMember.id === this.props.playerId) {
                console.log(member.id);
                squadMemberId = member.id;
            }
        })

        return squadMemberId;
    }

    checkInFetch = async (currTime, endTime) => {
        const squadMemberId = this.getSquadMemberId();

        if(squadMemberId === 0) {
            console.log("something went wrong ");
            return;
        }
        try {
            const resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.gameId}/squad/${this.state.squadId}/check-in`, {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken,
                },
                body: JSON.stringify({
                    startTime: currTime.toString(),
                    endTime: endTime.toString(),
                    lat: this.state.lat,
                    lng: this.state.lng,
                    objectiveTag: "dummy text",
                    gameId: this.state.gameId,
                    squadId: this.state.squadId,
                    squadMemberId: squadMemberId
                })
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });
            console.log(resp);
            if (resp[0] === 200) {
                console.log("added checkIn marker succsessully")
            }

        } catch (e) {
            console.log(e);
        }
    }

    render() {
        let squadMemberList;
        let squadName;

        if (this.state.squad !== null) {
            squadMemberList = this.state.squad.squadMemberList.map(squadMember => {
                return <SquadMember member={squadMember} />
            });
            squadName = <p>{this.state.squad.squadName}</p>
        }

        return (
            <div>
                {squadName}
                <div className={Styles.Cards}>
                    {squadMemberList}
                </div>
                {this.props.gameState !== "Complete" && 
                <div>
                <button className="btn" onClick={this.handleLeave} >Leave</button>
                <button className="btn" onClick={this.handleCheckIn} >Add CheckIn Marker</button>
                </div>} 
                
            </div>
        );
    }
}
export default SquadDetail;