import React from 'react';
import Styles from '../../../views/Home/Home.module.css';
import SquadComponent from '../SquadComponent/SquadComponent';
import CreateSquad from '../CreateSquad/CreateSquad';

class SquadList extends React.Component {

    state = {
        SquadList: [],
        gameId: this.props.gameId,
        playerId: this.props.playerId,
        accessToken: sessionStorage.getItem("accessToken"),
    }

    componentDidMount = async () => {

        //TODO test variables, need to bring in game props 
        const gameId = 1;

        try {
            const resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.gameId}/squad`, {

                method:"GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken
                }
            }).then(resp => resp.json());

            console.log("SquadList component did mount");
            console.log(resp);

            this.setState({
                SquadList: resp,
            })

        } catch (e) {
            console.log(e)
        }
    }

    handleClick = async (squadId) => {
        console.log("click");
        let resp
        try {
            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.gameId}/squad/${squadId}/join`, {
                method:"POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken
                },
                body: JSON.stringify({
                    id: 0,
                })
            }).then(resp => {return Promise.all([resp.status, resp.json()])})

        } catch(e) {
            console.log(e);
        }

        console.log(resp);
        if(resp && resp[0] === 200) {
            this.props.updateSquadID(resp[1].squadId);
        }
    }

    updateSquadID = (squadId) => {
        console.log("SquadId " + squadId)
        this.props.updateSquadID(squadId);
    }


    render() {

        console.log("SquadList loggs game");
        console.log(this.state.SquadList);
        const SquadComponentList = this.props.squadList.map(squad => {
            console.log(squad);
            return <SquadComponent squad={squad} handleClick={this.handleClick} />
        });

        return (
            <div className={Styles.Cards}>
                {SquadComponentList}
                {this.props.gameState !== "Complete" && <CreateSquad gameId={this.state.gameId} playerId={this.state.playerId} updateSquadID={this.updateSquadID} />}
                
                
            </div>
        );
    }
}

export default SquadList;