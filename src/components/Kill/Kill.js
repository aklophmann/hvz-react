import React from 'react';
import Styles from '../../views/Home/Home.module.css';

class Kill extends React.Component {

    state = {
        kill: this.props.kill,
    }

    render() {
        const gameOrPlayerKills = this.props.name ? this.props.name + " killed " : "You killed ";

        const story = this.props.kill.story == null ? <div></div> : <div> {"Story: " + this.state.kill.story} <br/> </div>
        const position = this.props.kill.lat == null ? <div></div> : <div> {"Position: lat - " + this.state.kill.lat + " lng - " + this.state.kill.lng}</div>

        return (
            <div>
                {gameOrPlayerKills + this.state.kill.victim.username + " at " + this.state.kill.timeOfDeath} <br/>
                {story}
                {position}
            </div>
        );
    }
}

export default Kill;