import React from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class UpdateMissionModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            missionName: '',
            faction: 'human',
            description: '',
            startDate: new Date(`${this.props.location.m.startDate}`),
            endDate: new Date(`${this.props.location.m.endDate}`)
        }
        this.addMissionName = this.addMissionName.bind(this);
        this.addFaction = this.addFaction.bind(this);
        this.addDescription = this.addDescription.bind(this);
        this.addStartDate = this.addStartDate.bind(this);
        this.addEndDate = this.addEndDate.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if(JSON.parse(sessionStorage.getItem('user')).role != 'ROLE_ADMIN') {
            this.props.history.push('../');
        }

        if(this.props.location.m.isHumanFaction) {
            this.setState({
                faction: 'human'
            })
        } else {
            this.setState({
                faction: 'zombie'
            })
        }
        
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        console.log(this.props.location.m.id);

        let resp;
        let isHuman;
        if(this.state.faction == 'human') {
            isHuman = true;
        } else {
            isHuman = false
        }

        try {
            const accessToken = sessionStorage.getItem("accessToken");

            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.props.location.m.gameId}/mission/${this.props.location.m.id}/`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + accessToken
                },
                body: JSON.stringify({
                    missionName: this.state.missionName,
                    isHumanFaction: isHuman,
                    isZombieFaction: !isHuman,
                    missionDescription: this.state.description,
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    gameId: this.props.location.m.gameId,
                    lat: this.props.location.m.lat,
                    lng: this.props.location.m.lng,
                })
            }).then(resp => {return Promise.all([resp.status, resp.json()])});
            console.log(resp);
        } catch (e) {
            console.error(e);
        }

        if (resp) {
            this.props.history.push(`../gamedetails/${this.props.location.m.gameId}`);
        }
    }

    addMissionName(e) {
        this.setState({ missionName: e.target.value });
    }

    addFaction(e) {
        this.setState({ faction: e.target.value });
    }

    addDescription(e) {
        this.setState({ description: e.target.value });
    }

    addStartDate(date) {
        let sDate = new Date(date);
        this.setState({ startDate: sDate });
    }

    addEndDate(date) {
        let sDate = new Date(date);
        this.setState({ endDate: sDate });
    }

    render() {

        const backdropStyle = {
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0,3',
            padding: 50
        };

        const modalStyle = {
            backgroundColor: '#fff',
            borderRadius: 5,
            color: '#000',
            maxWidth: 500,
            minHeight: 300,
            margin: '0 auto',
            padding: 30
        };

        return (
            <div style={backdropStyle}>
                <div style={modalStyle}>
                    <label>Update Mission</label>

                    <div className="footer">
                        <form onSubmit={this.handleSubmit}>
                            <input name="missionName" placeholder={`${this.props.location.m.missionName}`} required value={this.state.missionName} onChange={this.addMissionName}></input>
                            <hr />
                            <label id="faction_type">Faction type:</label>
                            <select value={this.state.faction} onChange={this.addFaction}>
                                <option value="human">Human</option>
                                <option value="zombie">Zombie</option>

                            </select>
                            <hr />
                            <label>Mission description</label>
                            <br />
                            <textarea id="mission_description" placeholder={`${this.props.location.m.missionDescription}`} value={this.state.description} onChange={this.addDescription} style={{ border: "1px solid #888" }} />
                            <hr />
                            <br />
                            <label id="datepicker">Date:</label>
                            <br />
                            <DatePicker
                                selected={this.state.startDate}
                                onChange={this.addStartDate} />
                            <br />
                            <DatePicker
                                selected={this.state.endDate}
                                onChange={this.addEndDate} />
                            <hr />
                            <br />
                            <input type="submit" className="btn" value="Update Mission" />
                            <button className="btn" onClick={() => {this.props.history.push(`../gamedetails/${this.props.location.m.gameId}`)}}>Cancel</button>
                        </form>
                    </div>
                </div>
            </div >
        );
    }
}

export default UpdateMissionModal;