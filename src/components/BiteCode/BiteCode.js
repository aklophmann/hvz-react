import React from 'react';
import styles from './BiteCode.module.css';

function BiteCode(props) {

    return (
        <div className="mt-3">
            <h4>BiteCode:</h4>
            <h4>{props.biteCode}</h4>
        </div>
    );
}

export default BiteCode;