import React from 'react';
import {FormErrors} from '../FormErrors/FormErrors'

class Login extends React.Component{
    
    state = {
        register: false,
        username: '',
        password: '',
        ctrlPassword: '',

        formErrors: {username: '', password: ''},
        usernameValid: false,
        passwordValid: false,
        ctrlPasswordValid: false,
        formValid: false,
        wrongUsernamePassword: '',

        link: '../'
    }
    componentDidMount = () => {
        console.log(this.props.location.state)
    }
    
    toggleRegister = () =>{
        this.setState({
            register: !this.state.register
        });
    }

    handleUserInput = (e) =>{
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
            () => {this.validateInput(name, value)});
    }

    validateInput = (fieldName, value) =>{
        let validationErrors = this.state.formErrors;
        let usernameValid = this.state.usernameValid;
        let passwordValid = this.state.passwordValid;
        let ctrlPasswordValid = this.state.ctrlPasswordValid

        switch(fieldName){
            case 'username':
                usernameValid = value.match(/^[a-zA-Z0-9_-]*$/);
                validationErrors.username = usernameValid ? '' : 'is invalid';
                break;
            case 'password':
                passwordValid = value.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/);
                validationErrors.password = passwordValid ? '' : 'is invalid';
                break;
            case 'ctrlPassword':
                value === this.state.password ? ctrlPasswordValid = true : ctrlPasswordValid = false;
                validationErrors.ctrlPassword = ctrlPasswordValid ? '' : 'do not match';
                break;
            default: break;
        }
        this.setState({
            formErrors: validationErrors,
            usernameValid: usernameValid,
            passwordValid: passwordValid,
            ctrlPasswordValid: ctrlPasswordValid
        }, this.validateForm);
    }

    validateForm = () => {
        if (this.state.register){
            this.setState({
                formValid: this.state.usernameValid && this.state.passwordValid && this.state.ctrlPasswordValid
            })
        }
        else{
            this.setState({
                formValid: this.state.usernameValid && this.state.passwordValid
            });
        }
    }

    submitHandler = async (e) => {
        e.persist(); 
        e.preventDefault();
        if (this.state.register && this.state.formValid){
            let resp;
            try{
                resp = await fetch('https://hvz-backend.herokuapp.com/user/register', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password,
                    role: "",
                    })
                }).then(resp => resp.json());
                console.log("------test------");
                console.log(resp);
                
            }
            catch(e){
                console.error(e);
            }
            this.setState({register: false});
            this.submitHandler(e);
        }
        else if (!this.state.register && this.state.formValid){
            let resp;
            try{
                resp = await fetch('https://hvz-backend.herokuapp.com/user/login', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password,
                    role: "",
                    })
                }).then(resp => {return Promise.all([resp.status, resp.json()])});
                console.log("------test------");
                console.log(resp);
            }
            catch(e){
                console.error(e);
            }
            if(resp[0] == 200){
                sessionStorage.setItem("accessToken", resp[1][0]);
                sessionStorage.setItem("user", JSON.stringify(resp[1][1]));
                if (this.props.location.state && this.props.location.state.location){
                    window.location.replace(this.props.location.state.location);
                }else{
                    window.location.replace("../");
                }
            }
        }
        else {
            this.setState({wrongUsernamePassword: 'Please enter a valid username and password'});
        }

    }
    render(){
        //const location = this.props.location.state.location;
        
        return(
            <div>
                <div>
                    {!this.state.register &&
                    <h3>Login</h3>}
                    {this.state.register &&
                    <h3>Register</h3>}
                    
                    <form onSubmit={this.submitHandler}>
                        <input type="text" name="username" placeholder="Username" onBlur={this.handleUserInput}></input><br/>
                        <input type="password" name="password" placeholder="Password" onChange={this.handleUserInput}></input><br/>
                        
                        {!this.state.register &&
                        <input type="submit" className="btn" disabled={!this.state.formValid} value="Login"></input>}
                        {this.state.register &&
                            <div>
                                <input type="password" name="ctrlPassword" placeholder="Repeat Password" onChange={this.handleUserInput}></input> <br/>
                                
                                <input type="submit" className="btn" disabled={!this.state.formValid} value="Register" ></input>
                            </div>
                        }
                    </form>
                    <div className="panel panel-default">
                        <FormErrors formErrors={this.state.formErrors} style={{color:'red'}}/>
                    </div>
                    
                    {!this.state.register &&
                    <div>
                        Don't have a user?<br/>
                        <u onClick={this.toggleRegister}>Register here</u>
                    </div>}
                    {this.state.register &&
                    <div>
                        Allready have a user? <br/>
                        <u onClick={this.toggleRegister}>Login here</u>
                    </div>}
                </div>
            </div>
        );
    }
}
export default Login;