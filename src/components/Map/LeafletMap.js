import React from 'react';
import { Map, TileLayer, Marker, Popup, CircleMarker, Circle } from 'react-leaflet';
import L from 'leaflet';
import './GameMap.css';
import { LatLng } from 'leaflet';
import CreateMissionModal from './CreateMissionModal/CreateMissionModal';
import { Link } from 'react-router-dom';
import styles from '../../views/Home/Home.module.css';


class LeafletMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPopup: false,
            expandPopup: false,
            currentPos: null,
            showModal: false,
            markers: [],
            game: this.props.game,
            currMission: null,
            gravestones: null, //gravestones for kills
            showInfo: null
        };
        this.handleClick = this.handleClick.bind(this);
    }


    componentDidMount = async () => {
        const game = this.props.game;
        let resp;

        const accessToken = sessionStorage.getItem("accessToken");
        try {
            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${game.id}/mission/`, {

                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + accessToken
                }
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });

            //console.log(this.state.markers);

        } catch (e) {
            console.error(e);
        }
        if (resp && resp[0] == 200) {
            this.setState({ markers: resp[1] });
            this.setState({ gravestones: this.props.game.killsList });
        }
    }

    handleClick(e) {
        this.setState({ currentPos: null });
        this.setState({ currentPos: e.latlng });
        console.log("map clicked");
    }

    addMission = () => {
        this.props.history.push({ pathname: '../newmission', pos: this.state.currentPos, game: this.state.game });
    }

    deleteMission = async (mid) => {
        let resp;
        const accessToken = sessionStorage.getItem("accessToken");

        resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.props.game.id}/mission/${mid}/`, {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + accessToken
            }
        }).then(resp => { return Promise.all([resp.status, resp.json]) });

        if (resp[0] == 200) {
            let missionIndex;
            let arrCopy = this.state.markers.slice();

            let check = arrCopy.forEach((mission, index) => {
                if (mission.id == mid) {
                    missionIndex = index;
                }
            })
            arrCopy.splice(missionIndex, 1);
            this.setState({
                markers: arrCopy
            });
        }
    }

    showMissionInfo = (m) => {
        this.setState({
            showInfo: m
        })

        console.log(this.state.showInfo);
    }

    getSquadMemberList = async (squadId) => {
        ///
        const game = this.props.game;
        let resp;

        const accessToken = sessionStorage.getItem("accessToken");
        try {
            resp = await fetch(`http://localhost:8080/game/${game.id}/squad/${squadId}/members`, {

                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + accessToken
                }
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });
        } catch (e) {
            console.error(e);
        }
        if (resp && resp[0] == 200) {
            return resp[1];
        }
    }

    render() {
        // Map attributes
        const maptiles = 'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=9jsoV6h80KY0wgvpfOuc';
        const mapAttribution = '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>';
        let mapCenter = [this.props.game.latitude, this.props.game.longitude];
        const zoomLevel = 12;

        const user = JSON.parse(sessionStorage.getItem('user'));

        // Get missions to set mission markers
        let missionMarkers;
        let missionIcon = L.icon({
            iconUrl: require('../../icons/pointer-1915456_960_720.png'),
            iconSize: [20, 30]
        })

        if (this.state.markers.length > 0) {
            missionMarkers = this.state.markers.map(mission =>
                <Marker key={mission.id} position={new LatLng(mission.lat, mission.lng)} icon={missionIcon}>
                    <Popup>
                        <p id="info-title">{mission.missionName}</p>
                        <hr />
                        <button id="button-style" className="btn" onClick={() => { this.showMissionInfo(mission) }}>More info</button>
                        {user.role == 'ROLE_ADMIN' &&
                            <div>
                                <Link id="button-style" to={{ pathname: '../updatemission', m: mission }} className="btn">Update Mission</Link>
                                <br />
                                <button id="button-style" className="btn" onClick={() => { this.deleteMission(mission.id) }}>Delete Mission</button>
                            </div>
                        }
                    </Popup>
                </Marker>
            );
        }

        // Get kills to set gravestones
        let gravestoneIcon = L.icon({
            iconUrl: require('../../icons/tombstone-and-grave-vector-art.png'),
            iconSize: [35, 30]
        })

        let graves;
        if (this.state.gravestones != null && this.state.gravestones.length) {
            graves = this.state.gravestones.map(kill =>
                <Marker key={kill.id} position={new LatLng(kill.lat, kill.lng)} icon={gravestoneIcon}>
                    <Popup>
                        <p id="info-title">Player: {kill.victim.username}</p>
                        <hr />
                        {kill.story}
                    </Popup>
                </Marker>)
        }

        // Squad check in marker
        let squadCheckInIcon = L.icon({
            iconUrl: require('../../icons/position-icon.png'),
            iconSize: [30, 30]
        })

        let squadlist = this.props.game.squadList;
        let squadcheckinmarkers;
        let squadcheck;

        if (squadlist.length) {
            squadcheckinmarkers = squadlist.forEach(squad => {
                if(this.props.player && squad.id == this.props.player.squadId){
                    if (squad.squadCheckInList.length) {
                        squadcheck = squad.squadCheckInList.map(element => {
                            
                            let memberName;
                            squad.squadMemberList.forEach(user => {
                                if (user.id == element.squadMemberId){
                                    memberName = user.squadMember.username;
                                }
                            })
                            
                            return (<Marker key={element.id} position={new LatLng(element.lat, element.lng)} icon={squadCheckInIcon}>
                                <Popup>
                                    {memberName}
                                </Popup>
                            </Marker>);
                        });
                    }
                }
            });
        }

        return (
            <div>
                <div className="map">
                    <div className="container">
                        {/* MAP */}
                        <Map ref='map' style={{ height: '300px', width: '96%', marginLeft: '15px' }} center={mapCenter} zoom={zoomLevel} onClick={this.handleClick}>
                            <TileLayer
                                url={maptiles}
                                attribution={mapAttribution}
                            />
                            <Circle center={new LatLng(this.props.game.latitude, this.props.game.longitude)} radius={1000} color="red"></Circle>

                            {/* Mission markers */}
                            {missionMarkers}

                            {/* Gravestone markers */}
                            {graves}

                            {/* Squad Check In markers */}
                            {squadcheck}

                            {/* Add button */}
                            {this.state.currentPos && user.role == 'ROLE_ADMIN' &&
                                <Popup position={this.state.currentPos}>
                                    <Link id="button-style" to={{ pathname: '../newmission', pos: this.state.currentPos, game: this.state.game }} className="btn">Add Mission</Link>
                                </Popup>}
                        </Map>
                    </div>
                </div>
                {/* INFO BOX */}
                <div className="container">
                    <div className={styles.Cards}>
                        <div className={`${styles.gdCard} ${styles.mapInfoBox}`}>
                            {this.state.showInfo != null &&
                                <div>
                                    <p id="info-title">{this.state.showInfo.missionName}</p>
                                    <hr />

                                    <br />
                                    <div className="row">
                                        <div className="col-5">
                                            <p>Mission description</p>
                                        </div>
                                        <div className="col-7">
                                            {this.state.showInfo.missionDescription}

                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>

            </div >
        );
    }
}

export default LeafletMap;