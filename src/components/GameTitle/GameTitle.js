import React from 'react';
import {Link} from 'react-router-dom'
import styles from '../../views/Home/Home.module.css'

class GameTitle extends React.Component{
    

    render(){
        const game = this.props.game;
        return(
                <div>
                    <h3>{game.name}</h3>
                    <p>{game.startDate}</p>
                    <p>{game.description}</p>
                    <h5>See the rules <u><Link to="../rules">Here</Link></u></h5>
                </div>
        );
    }
}
export default GameTitle;