import React from 'react';
import Styles from '../../../views/Home/Home.module.css';

class SqaudComponent extends React.Component {
    state ={
        expandCard: false,
        deceasedPlayers: 0
    }
    
    componentDidMount = () => {
        this.props.squad.squadMemberList.forEach(squadMember => {
            if(!squadMember.squadMember.isHuman) {
                this.setState({deceasedPlayers: this.state.deceasedPlayers + 1});
            }
        });
    }

    handleClick = () => {
        this.props.handleClick(this.props.squad.id);
    }

    toggleCard = () => {
        this.setState({expandCard: !this.state.expandCard});
    }

    render (){
        return( 
            <div className={Styles.Card} onClick={this.toggleCard}>
                <p>Squad name: {this.props.squad.squadName}</p>
                {this.state.expandCard &&
                    <div>
                        <p>Number of players: {this.props.squad.squadMemberList.length}</p>
                        <p>Number of deceased players: {this.state.deceasedPlayers}</p>
                        <button className="btn" onClick={this.handleClick} >Join Squad</button>
                    </div>
                }
            </div>

        );
    }
} 

export default SqaudComponent;