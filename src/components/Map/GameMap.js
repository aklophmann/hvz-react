import React, { Component } from 'react';
import { render } from 'react-dom';
import L from 'leaflet';
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import { Map } from 'react-leaflet';
import './GameMap.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimePicker from "react-time-picker";

const maptiles = 'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=qm2OZ0KQzznHTMyyAuet';
const mapAttribution = '<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>';
const mapCenter = [59.911491, 10.757933];
const zoomLevel = 12;
var map;

class GameMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //Mission details
            name: '',
            faction: 'human',
            description: '',
            startDate: new Date(),
            endDate: new Date(),
            startTime: '',
            endTime: '',
            gameid: '1',
            showModal: false
        };

        this.addFaction = this.addFaction.bind(this);
        this.addDescription = this.addDescription.bind(this);
        this.addMissionName = this.addMissionName.bind(this);
        this.addStartDate = this.addStartDate.bind(this);
        this.addEndDate = this.addEndDate.bind(this);
        this.addStartTime = this.addStartTime.bind(this);
        this.addEndTime = this.addEndTime.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        alert('faction: ' + this.state.faction + '\ndescription: ' + this.state.description + '\nmission name: ' + this.state.name +
            '\nstartdate: ' + this.state.startDate + '\nenddate: ' + this.state.endDate + '\nstarttime: ' + this.state.startTime +
            '\nendtime: ' + this.state.endTime);
        event.preventDefault();
        let isHuman;
        if (this.state.faction == 'human') {
            isHuman = true;
        } else {
            isHuman = false;
        }
        try {
            //TODO: gameid automatically
            const resp = await fetch('https://hvz-backend.herokuapp.com/game/1/mission', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNTcxMTQyMzI1LCJleHAiOjE1NzE3NDcxMjV9.4_GDmtZg-jDD-WdK18N6d8nodo6tOILZmSmWa7n8e4OHJ1c_zj4wi5Wp3Mjb5LqJEpmnBU5jQGi7sd7btDt7Bg'
                },
                body: JSON.stringify({
                    missionName: this.state.name,
                    isHumanFaction: isHuman,
                    isZombieFaction: !isHuman,
                    missionDescription: this.state.description,
                    startTime: this.state.startTime,
                    endTime: this.state.endTime,
                    gameId: 1,
                })
            }).then(resp => resp.json());
            console.log("TEST*********");
            console.log(resp);
        } catch (e) {
            console.error(e);
        }
    }

    addMissionName(event) {
        this.setState({ name: event.target.value });
    }

    addFaction(event) {
        this.setState({ faction: event.target.value });
    }

    addDescription(event) {
        this.setState({ description: event.target.value });
    }

    addStartDate(date) {
        this.setState({ startDate: date });
    }

    addEndDate(date) {
        this.setState({ endDate: date });
    }

    addStartTime(time) {
        this.setState({ startTime: time });
    }

    addEndTime(time) {
        this.setState({ endTime: time });
    }


    createMarker(lat, lng, description) {
        var marker = L.marker([lat, lng]).addTo(map);
        marker.bindPopup(description).openPopup();
    }

    mapClicked = () => {
        console.log("map clicked")
        this.setState({
            showModal: true
        });
    }
    componentDidMount = () => {
        /* map = L.map('mapid').setView(mapCenter, zoomLevel);

        L.tileLayer(maptiles, {
            attribution: mapAttribution,
        }).addTo(map);

        map.on('click', function (e) { //Get the lat and long
            GameMap.mapClicked();
            //alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
        }); */

        /*
        1. Click on a position on the map
        2. Popup a modal that asks if you want to add a mission here
        3. If no exit, if yes then a form should popup 
        */
        /*
        On click get a popup, do you wish to make a mission?
        If yes, get up a modal with form maybe, or just the form pops up
        After the information is filled, the mission is saved into the repo
        and then the marker is set with the clicked lat and lng in the map

        //Error when you want to set the marker outside the gamemap
        */
        // try {
        //     const resp = await fetch('https://hvz-backend.herokuapp.com/game/1/mission').then(resp => resp.json());
        //     let missionList = resp.slice()
        //     missionList.forEach(mission => {
        //         var tmpMarker = L.marker([])
        //     })
        // }
        /*
        Get the list of games specific if the user is zombie or human
        Then create markers 
        */

        /* 
                //Make dummy markers
                var marker = L.marker([59.911491, 10.757933]).addTo(map);
                var marker2 = L.marker([59.95, 10.757933]).addTo(map);
        
        
        
                //Make dummy game areas
                var circle = L.circle([59.911491, 10.757933], {
                    color: 'red',
                    fillColor: '#f03',
                    fillOpacity: 0.1,
                    radius: 200
                }).addTo(map);
        
                marker.bindPopup("Mission 1").openPopup();
                marker2.bindPopup("Mission 2").openPopup();
                //circle.bindPopup("Hey There! I'm a circle.").openPopup(); */

    }


    render() {
        return <div>
            {/* <div className="container" id="mapid"></div> */}
            <div>
                <Map center={this.mapCenter} zoom={this.zoomLevel} onClick={this.mapClicked}>
                    <TileLayer url={'http://{s}.tile.osm.org/{z}/{x}/{y}.png'} />
                    {this.state.currentPos &&
                        <Marker draggable={true}>
                            <Popup>
                                Current location: <pre></pre>
                            </Popup>
                        </Marker>
                    }
                </Map>
            </div>

            <div id="mymodal" class="modal fade" tabindex="-1" role="dialog">
                {this.state.showModal &&
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                <p>One fine body&hellip;</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Confirm</button>
                            </div>
                        </div>
                    </div>}
            </div>


            <form onSubmit={this.handleSubmit.bind(this)}>
                <label>Mission Name</label>
                <br />
                <input name="missionName" placeholder="Mission name" required value={this.state.name} onChange={this.addMissionName}></input>
                <hr />
                <label id="faction_type">Faction type:</label>
                <select value={this.state.faction} onChange={this.addFaction}>
                    <option value="human">Human</option>
                    <option value="zombie">Zombie</option>
                </select>

                <hr />
                <label>Mission description</label>
                <br />
                <textarea id="mission_description" value={this.state.description} onChange={this.addDescription} />
                <hr />
                <br />
                <label id="datepicker">Date:</label>
                <br />
                <DatePicker
                    selected={this.state.startDate}
                    onChange={this.addStartDate} />
                <br />
                <DatePicker
                    selected={this.state.endDate}
                    onChange={this.addEndDate} />
                <br />
                <hr />
                <label id="timepicker">Time:</label>
                <br />
                <TimePicker
                    value={this.state.startTime}
                    onChange={this.addStartTime} />
                <br />
                <TimePicker
                    value={this.state.endTime}
                    onChange={this.addEndTime} />
                <hr />
                <input type="submit" value="Create Mission" />
            </form>
        </div>



    }
}

export default GameMap;
