import React from 'react'

class BiteCodeEntry extends React.Component {

    state = {
        biteCode: null,
        story: null,
        showMessage: false,
        validInput: false,
        statusCode: null,
        playerId: this.props.playerId,
        gameId: this.props.gameId,
        token: null,
        addGravestone: false,
        lng: null,
        lat: null,
        accessToken: sessionStorage.getItem("accessToken"),

    }

    

    handleUserInput = (event) => {
        const name = event.target.name;
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        this.setState({
            [name]: value,
            showMessage: false,
        })

        if(name === 'addGravestone' && value === true) {
            this.getGeoLocation();
        } else if (name === 'addGravestone' && value === false){
            this.setState({
                lng: null,
                lat: null,
            })
        }

        if(name === 'biteCode') {
            this.validateInput(value);
        }

    }

    validateInput = (value) => {
        if(value.match(/^[0-9]{6,6}$/)) {
            this.setState({
                validInput: true
            })
        } else {
            this.setState({
                validInput: false
            })
        }
        
    }

    getGeoLocation = () => {
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.printLocationInfo);
        } else {
            console.log("no");
        }
    }

    printLocationInfo = (position) => {
        const lng = position.coords.longitude;
        const lat = position.coords.latitude;

        this.setState({
            lng: lng,
            lat: lat,
        })

        console.log("Longitude: " + lng + "- Latitude: " + lat);

    }

    submitHandler = async (event) => {
        event.preventDefault()


        console.log("Pressed button. Bitecode er " + this.state.biteCode)

        try{
            const resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.gameId}/kill`, {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    'Authorization': 'Bearer ' + this.state.accessToken,
                    "biteCode": this.state.biteCode,
                },
                body: JSON.stringify({
                    killerId: this.state.playerId,
                    story: this.state.story,
                    lat: this.state.lat,
                    lng: this.state.lng,
                })
            }).then(resp => { return Promise.all([resp.status, resp.json()])});

            this.setState({
                showMessage: true,
                statusCode: resp[0],
            })
            console.log(resp);

        } catch(e) {
            console.log(e)
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.submitHandler}>
                    <input type="number" name="biteCode" placeholder="Enter Bite Code" onChange={this.handleUserInput}/><br/>
                    <input type="text" name="story" placeholder="Enter optional story" onChange={this.handleUserInput}/><br/>
                    <input type="checkbox" className="form-check-input" name="addGravestone" checked={this.state.addGravestone} onChange={this.handleUserInput} />
                    <label>Add gravestone</label><br/>
                    <input type="submit" className="btn" disabled={!this.state.validInput} value="Enter" />
                </form>
                {
                    this.state.showMessage &&
                    <div> {
                        this.state.statusCode === 200 && 
                        <div> 
                            Kill logged succesfully
                        </div> ||
                        this.state.statusCode !== 200 &&
                        <div>
                            Kill was not logged succesfully
                        </div>
                        } </div>
                }
            </div>
        );
    }
}

export default BiteCodeEntry;