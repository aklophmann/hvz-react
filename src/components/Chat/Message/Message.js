import React from 'react';

import styles from '../Chat.module.css';

const Message = (props) =>{
    const message = props.message;
    return(
        <div className="float-left pl-1 w-100">
            <p className={`${styles.username} d-inline float-left`}>{message.username}: </p>
            <p className="d-inline pl-2 message"> {message.message}</p>
        </div>
    );
}
export default Message;