import React from 'react';
import Styles from '../../../views/Home/Home.module.css';

class CreateSquad extends React.Component {

    state = {
        squadName: "",
        gameId: this.props.gameId,
        playerId: this.props.playerId,
        accessToken: sessionStorage.getItem("accessToken"),
    }

    handleInput = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        console.log(value);

        this.setState({
            [name]: value,
        })
    }

    onSubmit = async(event) => {
        event.preventDefault()
        let resp;
        try{
            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.gameId}/squad`,{
                method:"POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken,
                    'player_id': this.state.playerId,
                },
                body: JSON.stringify({
                    squadName: this.state.squadName,
                    gameId: this.state.gameId,
                })
            }).then(resp => {return Promise.all([resp.status, resp.json()])});
            console.log(resp);
        }
        catch(e){console.error(e)}
        console.log(resp);
        this.props.updateSquadID(resp[1].id);
    }

    render() {
        return (
            <div className={Styles.Card}>
                <form onSubmit={this.onSubmit}>
                    <input type="text" name="squadName" placeholder="Enter squad name" onChange={this.handleInput} />
                    <input type="submit" className="btn" value="Create new squad" />
                </form>
            </div>
        );
    }

}

export default CreateSquad;