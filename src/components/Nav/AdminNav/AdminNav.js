import React from 'react';
import {Link} from 'react-router-dom';

function UserNav() {
    const signOut = () => {
        sessionStorage.clear();
        window.location.replace('/');
    }
    return(
        <div className="pt-4">
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" className="dropdown-toggle">Admin</a>
                <ul className="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <p onClick={signOut}>Sign Out</p>
                        <Link to="../newgame">New Game</Link>
                    </li>
                </ul>
            </li>
        </div>
    );
}

export default UserNav;