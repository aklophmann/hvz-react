import React from 'react';

import styles from './Chat.module.css';
import Message from './Message/Message';
import Notification from '../../icons/notification.png';

class Chat extends React.Component{
    state = {
        toggle: true,
        chatInput: '',
        gameId: this.props.game.id,
        player: this.props.player,
        endpoint: `/game/1/AllChat/`,
        messageList: [],
        prevMessageList: [],
        newMessage: 0,
        isHuman: true,
        isZombie: true,
        globalChatActive: true,
        humanChatActive: false,
        zombieChatActive: false,
        squadChatActive: false
    }

    componentDidMount=  async () => {
        await this.getMessages(this.state.endpoint).then(this.scrollToBottom());
        this.interval = setInterval(() => {
          this.getMessages(this.state.endpoint).then(() => {
              if (this.state.messageList.length > this.state.prevMessageList.length){
                  this.setState({
                      newMessage: this.state.newMessage+1
                  })
              }
              this.scrollToBottom()
            });
          
        }, 5000);
        
    }

    getMessages = async (endpoint) => {
        let resp;
        try{
            resp = await fetch(`https://hvz-backend.herokuapp.com${endpoint}`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('accessToken')
                }
            }).then(resp => {return Promise.all([resp.status, resp.json()])});
        }catch(e){
            console.error(e);
        }
        if(resp && resp[0] == 200){
            this.setState({
                prevMessageList: this.state.messageList,
                messageList: resp[1]
            })
        }
    }

    chatSelector = (endpoint) => {
        let urlEndpoint;
        switch (endpoint){
            case 'squad':
                urlEndpoint = `/game/${this.state.gameId}/squad/${this.state.playersquadId}/chat`;
                this.setState({
                    isHuman: false,
                    isZombie: false,
                    globalChatActive: false,
                    humanChatActive: false,
                    zombieChatActive: false,
                    squadChatActive: true,
                    endpoint: `/game/${this.state.gameId}/squad/${this.state.player.squadId}/chat`
                });
                break;
            case 'AllChat':
                urlEndpoint = `/game/${this.state.gameId}/AllChat/`;
                this.setState({
                    isHuman: true,
                    isZombie: true,
                    globalChatActive: true,
                    humanChatActive: false,
                    zombieChatActive: false,
                    squadChatActive: false,
                    endpoint: `/game/${this.state.gameId}/AllChat/`
                });
                break;
            case 'Human':
                urlEndpoint = `/game/${this.state.gameId}/factionChat/`;
                this.setState({
                    isHuman:true,
                    isZombie: false,
                    globalChatActive: false,
                    humanChatActive: true,
                    zombieChatActive: false,
                    squadChatActive: false,
                    endpoint: `/game/${this.state.gameId}/factionChat/`
                });
                break;
            case 'Zombie':
                urlEndpoint = `/game/${this.state.gameId}/factionChat/`;
                this.setState({
                    isHuman: false,
                    isZombie:true,
                    globalChatActive: false,
                    humanChatActive: false,
                    zombieChatActive: true,
                    squadChatActive: false,
                    endpoint: `/game/${this.state.gameId}/factionChat/`
                });
                break;
            default:
                break;
        }
        this.getMessages(urlEndpoint);
    }

    inputHandeler = (e) =>{
        this.setState({
            chatInput: e.target.value
        });
    }

    sendMessage = async() => {
        let endpoint;
        let resp;
        
        if (this.state.isHuman == false && this.state.isZombie == false && this.state.player.squadId){
            endpoint = `/game/${this.state.gameId}/squad/${this.state.player.squadId}/chat/`;
        }
        else{
            endpoint = `/game/${this.state.gameId}/chat`;
        }
        try{
            resp = await fetch(`https://hvz-backend.herokuapp.com${endpoint}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + sessionStorage.getItem('accessToken')
                },
                body: JSON.stringify({
                    'message': this.state.chatInput,
                    'isHuman': this.state.isHuman,
                    'isZombie': this.state.isZombie,
                })
            }).then(resp => Promise.all([resp.status, resp.json()]));
        }
        catch(e){
            console.error(e);
        }
        console.log(resp);
        

        this.getMessages(this.state.endpoint);
        this.setState({chatInput:''});
    }

    toggleChat = () => {
        this.setState({
            toggle: !this.state.toggle,
            newMessage: 0
        })
    }

    scrollToBottom = () => {
        if(!document.getElementById('scroll-body')==null){
            document.getElementById('scroll-body').scrollTop = document.getElementById('scroll-body').scrollHeight;
        }

        
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
    render(){
        console.log(this.state.newMessage);
        let messages;
        const chatBodyHeight = {height: '80vh'};

        const globalChat = <p className={`${styles.chatSelector} ${this.state.globalChatActive ? styles.chatActive: ''} d-inline`} onClick={() => this.chatSelector('AllChat')}>Global </p>;
        const humanChat = this.state.player && this.state.player.isHuman ? <p className={`${styles.chatSelector} ${this.state.humanChatActive ? styles.chatActive: ''} d-inline ml-2`} onClick={() => this.chatSelector('Human')}>Human </p> : '' ;
        const zombieChat = this.state.player && !this.state.player.isHuman ? <p className={`${styles.chatSelector} ${this.state.zombieChatActive ? styles.chatActive: ''} d-inline ml-2`} onClick={() => this.chatSelector('Zombie')}>Zombies </p> : '' ;
        const squadChat = this.state.player && this.state.player.isHuman && this.state.player.squadId && this.state.player.squadId > 0 ? (
            <p className={`${styles.chatSelector} ${this.state.squadChatActive ? styles.chatActive: ''} d-inline ml-2`} onClick={() => this.chatSelector('squad')}>Squad</p>) : '' ;


        if (this.state.messageList.length > 0){
            messages = this.state.messageList.map(message => {
                return <div><Message message={message} key={message.id} /><br/></div>
            })
        }
        return(
                <div className={`chat ${styles.sidebar} ${this.state.toggle ? styles.active : ''}`}>
                    {this.state.toggle && this.state.newMessage > 0 &&
                        <img src={Notification} id={styles.notification} className={styles.sidebarCollapse} alt=""></img>
                    }
                    <button className={`${styles.sidebarCollapse} btn`} onClick={this.toggleChat}>
                        Chat
                    </button>

                    <div className={`${styles.chatHeader} border`}>
                        {globalChat}
                        {humanChat}
                        {zombieChat}
                        {squadChat}
                    </div>
                    <div id="scroll-body" className={`${styles.chatBody}`} style={chatBodyHeight}>
                        {messages}
                    </div>
                    
                    <div className="chat-footer">
                        <input type="text" className={`${styles.chatInput} d-inline`} name="chatInput" value={this.state.chatInput} onChange={this.inputHandeler} placeholder="Send a message" />
                        <button className="btn d-inline" onClick={this.sendMessage}>Send</button>
                        
                    </div>
                </div>
        );
    }
}
export default Chat;