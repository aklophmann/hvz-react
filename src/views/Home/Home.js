import React from 'react';
import styles from './Home.module.css';
import GameCard from '../../components/GameCard/GameCard';

class Home extends React.Component{
    state = {
        games: [],
        myGames: [],
        gamesNotStarted: [],
        gamesInProgres: [],
        gamesCompleted: [],
        user: JSON.parse(sessionStorage.getItem("user"))
    };

    async componentDidMount(){
        this.fetchGame();
        this.interval = setInterval(() => {
             this.fetchGame();
         }, 5000); 
        console.log(this.state.game); 
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    createGame = () =>{
        this.props.history.push("../newgame");
    }

    fetchGame = async () => {
        try{
            const resp = await fetch('https://hvz-backend.herokuapp.com/game').then(resp => resp.json());
            let gameList = resp.slice()
            
            let myGames = [];
            let gns = [];
            let gip = [];
            let gc = [];
            let user = JSON.parse(sessionStorage.getItem('user'));
            if (user != null){
                user.Players.forEach(player => {
                    gameList.forEach(element => {
                        if (player.gameId == element.id){
                            myGames.push(element);
                        }
                    });
                });
            }
            gameList.forEach(element => {
                if (element.gameState == "Registration"){
                    gns.push(element);
                }else if (element.gameState == "In Progress"){
                    gip.push(element);
                }else if (element.gameState == "Complete"){
                    gc.push(element);
                }

            });
            this.setState({
                games: gameList,
                myGames: myGames,
                gamesNotStarted: gns,
                gamesInProgres: gip,
                gamesCompleted: gc
            });
            console.log("Updated")
        }
        catch(e){
            console.error(e);
        }
    }


    render(){
        const user = JSON.parse(sessionStorage.getItem('user'));
        const newGame = user && user.role == 'ROLE_ADMIN' ? <button className="btn" onClick={this.createGame}>New Game</button> : '';

        const cardsInProgress = this.state.gamesInProgres.map(card => {
            return <GameCard card={card} key={card.id} />;
        });
        const cardsNotStarted = this.state.gamesNotStarted.map(card => {
            return <GameCard card={card} key={card.id} />;
        });
        const cardsCompleted = this.state.gamesCompleted.map(card => {
            return <GameCard card={card} key={card.id} />;
        });

        const myCards = this.state.myGames.map(card => {
            return <GameCard card = {card} key={card.id} />;
        })

        

        return(
            <div className="container">
                {newGame}
                <div className={`${styles.Cards} row mt-4`}>
                    <div className={`${styles.gdCard} col-md-4 mr-2`}>
                        <h4>My Games:</h4><br/>
                        <div className={`${styles.Cards} row`}>
                            {user ? myCards : 'Sign in to see your games'}
                        </div>
                    </div>
                    <div className={`${styles.gdCard} col-md-4 ml-2`}>
                        <h4>Games in Progress:</h4> <br/>
                        <div className={`${styles.Cards} row`}>
                            {cardsInProgress}
                        </div>
                    </div>
                </div>
                <div className={`${styles.Cards} row mt-4`}>
                    <div className={`${styles.gdCard} col-md-4 mr-2`}>
                        <h4>Open Registration:</h4> <br/>
                        <div className={`${styles.Cards} row`}>
                            {cardsNotStarted}
                        </div>
                    </div>
                    <div className={`${styles.gdCard} col-md-4 ml-2`}>
                        <h4>Completed Games:</h4> <br/>
                        <div className={`${styles.Cards}`}>
                            {cardsCompleted}
                        </div>
                    </div>
                    
                </div>
                
            </div>
        );
    }

}
export default Home;