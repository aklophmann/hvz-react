import React from 'react';
import PropTypes from 'prop-types';
import '../GameMap.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class CreateMissionModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //Mission details
            name: '',
            faction: 'human',
            description: '',
            startDate: new Date(),
            endDate: new Date()
        };

        this.addFaction = this.addFaction.bind(this);
        this.addDescription = this.addDescription.bind(this);
        this.addMissionName = this.addMissionName.bind(this);
        this.addStartDate = this.addStartDate.bind(this);
        this.addEndDate = this.addEndDate.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if(JSON.parse(sessionStorage.getItem('user')).role != 'ROLE_ADMIN') {
            this.props.history.push('../');
        }
    }

    handleSubmit = async (event) => {
        // alert('faction: ' + this.state.faction + '\ndescription: ' + this.state.description + '\nmission name: ' + this.state.name +
            // '\nstartdate: ' + this.state.startDate + '\nenddate: ' + this.state.endDate + '\npos: ' + this.props.location.pos);
        event.preventDefault();
        const game = this.props.location.game;
        let resp;
        let isHuman;
        if (this.state.faction == 'human') {
            isHuman = true;
        } else {
            isHuman = false;
        }
        try {
            const accessToken = sessionStorage.getItem("accessToken");

            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${game.id}/mission`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + accessToken
                },
                body: JSON.stringify({
                    missionName: this.state.name,
                    isHumanFaction: isHuman,
                    isZombieFaction: !isHuman,
                    missionDescription: this.state.description,
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    gameId: game.id,
                    lat: this.props.location.pos.lat,
                    lng: this.props.location.pos.lng,
                })
            }).then(resp => {return Promise.all([resp.status, resp.json])});
            console.log("TEST*********");
            console.log(resp);
        } catch (e) {
            console.error(e);
        }

        if (resp) {
            this.props.history.push(`../gamedetails/${game.id}`);
        }
    }

    addMissionName(event) {
        this.setState({ name: event.target.value });
    }

    addFaction(event) {
        this.setState({ faction: event.target.value });
    }

    addDescription(event) {
        this.setState({ description: event.target.value });
    }

    addStartDate(date) {
        let sDate = new Date(date);
        this.setState({ startDate: sDate });
    }

    addEndDate(date) {
        let sDate = new Date(date);
        this.setState({ endDate: sDate });
    }

    render() {

        const backdropStyle = {
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0,3',
            padding: 50

        };

        const modalStyle = {
            backgroundColor: '#fff',
            borderRadius: 5,
            color: '#000',
            maxWidth: 500,
            minHeight: 300,
            margin: '0 auto',
            padding: 30
        };

        return (
            <div style={backdropStyle}>
                <div style={modalStyle}>
                    <label>Mission Name</label>

                    <div className="footer">
                        <form onSubmit={this.handleSubmit}>
                            <br />
                            <input name="missionName" placeholder="Mission name" required value={this.state.name} onChange={this.addMissionName}></input>
                            <hr />
                            <label id="faction_type">Faction type:</label>
                            <select value={this.state.faction} onChange={this.addFaction}>
                                <option value="human">Human</option>
                                <option value="zombie">Zombie</option>
                            </select>

                            <hr />
                            <label>Mission description</label>
                            <br />
                            <textarea id="mission_description" value={this.state.description} onChange={this.addDescription} style={{border:  "1px solid #888"}}/>
                            <hr />
                            <br />
                            <label id="datepicker">Date:</label>
                            <br />
                            <DatePicker
                                selected={this.state.startDate}
                                onChange={this.addStartDate} />
                            <br />
                            <DatePicker
                                selected={this.state.endDate}
                                onChange={this.addEndDate} />
                            <br />
                            <hr />
                            <input type="submit" className="btn" value="Create Mission" />
                            <button className="btn" onClick={() => {this.props.history.push(`../gamedetails/${this.props.location.game.id}`)}}>Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateMissionModal;