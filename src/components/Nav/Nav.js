import React from 'react';
import { Link } from 'react-router-dom';
import './Nav.css';
import UserNav from './UserNav/UserNav';
import AdminNav from './AdminNav/AdminNav';

class Nav extends React.Component{

    state = {
        toggle: false
    }
    toggleNav(){
        console.log('Toggle Nav')
        this.setState({
            toggle: !this.state.toggle
        });
    }
    signOut = () => {
        sessionStorage.clear();
        window.location.replace('/');
    }
    render(){
        let loggedInNav;
        const user = JSON.parse(sessionStorage.getItem("user"));
        if (user && user.role == "ROLE_USER"){
            loggedInNav = <li className="nav-item pt-5"><Link onClick={this.signOut} className="nav-link">Sign Out</Link></li>;
        }else if(user && user.role == "ROLE_ADMIN"){
            loggedInNav = <div>
                <li className="nav-item pt-5">
                    <Link onClick={this.signOut}>Sign Out</Link>
                </li>
                <li className="nav-item mt-3">
                    <Link className="nav-link" to="../newgame">New Game</Link>
                </li>
            </div>;
        }
        else{
            loggedInNav = <li className="nav-item pt-5">
            <Link to={`../login`} className="nav-link">Sign In</Link>
        </li>;
        }
        return(
            <div>
                <nav id="sidebar" className={this.state.toggle ? '' : 'active'}>
                    <button type="button" id="sidebarCollapse" className="btn" onClick={this.toggleNav.bind(this)}>
                        <span className="glyphicon glyphicon-menu-hamburger">Menu</span>
                    </button>
                    <ul className="list-unstyled components">
                        {loggedInNav}
                        <li className="nav-item mt-3">
                            <Link to={``} className="nav-link">Home</Link>
                        </li>
                        <li className="nav-item mt-3">
                            <Link to={`../rules`} className="nav-link">Rules</Link>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}
export default Nav;