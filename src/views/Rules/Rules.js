import React from 'react';
import styles from '../Home/Home.module.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class Rules extends React.Component{
    state = {
    }
    
    render(){
        
            return (
                <div className="container">
                    <h2>Rules</h2>
                    <p>
                        Humans vs. Zombies is a game of tag. All players begin as humans, 
                        and one is randomly chosen to be the “Original Zombie.” 
                        The Original Zombie tags human players and turns them into zombies. 
                        Zombies must tag and eat a human every 48 hours or they starve to death 
                        and are out of the game.
                    </p>
                    <h3>Objective</h3>
                    <p>The Zombies win when all human players have been tagged and turned into zombies.</p>
                    <p>The Humans win by surviving long enough for all of the zombies to starve.</p>
                    <h3>Equipment</h3>
                    <ul className='list-unstyled'>
                        <li>Bandana</li>
                        <li>Foam Dart Blaster</li>
                        <li>Marsmellow Launcher</li>
                        <li>Socks</li>
                        <li>Smart Phone</li>
                    </ul>
                    <h3>Safety Rules</h3>
                    <p>Rules created for the safety of all players are strictly enforced. Violation of safety rules will result in a ban from the game.</p>
                    <div>
                        <p>No realistic looking weaponry. Blasters must be brightly colored and have blaze-orange tips.</p>
                        <p>Blasters may not be visible inside of academic buildings or jobs on campus.</p>
                        <p>Players may not use cars or play where there is traffic.</p>
                        <p>Socks, Darts or Marshmallows must not hurt on impact.</p>
                    </div>
                    <h3>Human Rules</h3>
                    <p>Wearing a Bandanna: Humans must wear a headband around an arm or leg to identify them as players of the game. (This headband will come in handy when you become a zombie!)</p>
                    <p>Stunning a Zombie: Humans may stun a Zombie for 15 minutes by blasting them with a blaster or throwing a sock at them.</p>
                    <p>When Tagged By a Zombie: When tagged by a Zombie, a Human is required to distribute their ID card. One hour after being tagged, tie your bandanna around your head – you are now a member of the Zombie team! Go tag some Humans.</p>
                    <p>I.D. Number: Humans must keep an index card with their unique identification number on them at all times.</p>
                    <h3>Zombie Rules</h3>
                    <p>Feeding: Zombies must feed every 48 hours. A zombie feeds by reporting their tag on the website.</p>
                    <p>Tagging: A tag is a firm touch to any part of a Human. After tagging a Human the Zombie must collect their ID card and report the tag.</p>
                    <p>Getting Shot: When hit with a dart, a marshmallow, or a sock, a Zombie is stunned for 15 minutes. A stunned zombie may not interact with the game in any way. This includes shielding other zombies from bullets or continuing to run toward a human. If shot while stunned, the zombie’s stun timer is reset back to 15 minutes.</p>
                    <p>Wearing A Headband: Zombies must wear a bandanna around their heads at all times. The Original Zombie does not need to wear a headband.</p>
                </div>
            );
    }
}

export default Rules;