import React from 'react';
import styles from '../../views/Home/Home.module.css';

class PlayerCard extends React.Component{
    state = {
        player: this.props.player,
        isHuman: this.props.player.isHuman,
        isPatientZero: this.props.player.isPatientZero,
        squadId: this.props.player.squadId,
        edit: false,
        expandCard: false
    }

    changeIsHuman = () =>{
        this.setState({
            isHuman: !this.state.isHuman
        });
    }
    changeIsPatientZero = () =>{
        this.setState({
            isPatientZero: !this.state.isPatientZero
            
        });
    }

    changeSquad = (e) => {
        this.setState({
            squadId: e.target.value
        })
    }

    editClick = () => {
        this.setState({edit: !this.state.edit});
    }

    handleSubmit = async (game, player) => {
        let resp;
        try{
            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${game.id}/player/${player.id}`,{
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + sessionStorage.getItem('accessToken')
                },
                body: JSON.stringify({
                    'isHuman': this.state.isHuman,
                    'isPatientZero': this.state.isPatientZero,
                    'squadId': this.state.squadId
                })
            }).then(resp => {return Promise.all([resp.status, resp.json()])});
        } 
        catch(e){
            console.error(e);
        }
        console.log(resp);
        if (resp[0] == 200){
            this.setState({
                isHuman: resp[1].isHuman,
                isPatientZero: resp[1].isPatientZero,
                squadId: resp[1].squadId,
                edit: false
            })
        }
    }

    deletePlayer = async (game, player) => {
        let confirm = window.confirm(`Are you sure you want to delete player ${this.state.player.id}?`);
        if (confirm){
            let resp;
            try{
                resp = await fetch(`https://hvz-backend.herokuapp.com/game/${game.id}/player/${player.id}`,{
                    method: 'DELETE',
                    headers: {
                        'Authorization': 'Bearer ' + sessionStorage.getItem('accessToken')
                    }
                }).then(resp => {return Promise.all([resp.status, resp.json()])});
            } 
            catch(e){
                console.error(e);
            }
            console.log(resp);
            if(resp[0] == 200){
                this.props.deletedPlayer();
                console.log(`Deleted player ${player.id}`);
            }
        }
    }

    toggleCard = () => {
        this.setState({expandCard: !this.state.expandCard})
    }

    render(){
        console.log(this.state.isHuman + ' ' + this.state.isPatientZero + ' ' + this.state.squadId);
        console.log(this.state.player);
        const player = this.props.player;
        const game = this.props.game;
        const squads = game.squadList.map(squad =>{
            return <option value={squad.id} key={squad.id}>{squad.id}: {squad.squadName}</option>
        })
        const playerInfo = !this.state.edit ? (
            <div className="text-left">
                {player.id + ': '}
                {player.username}
                {this.state.expandCard &&
                    <div>
                        <p>{this.props.player.isHuman ? 'Human' : this.props.player.isPatientZero ? 'Patient Zero' : 'Zombie'}</p>
                        <p>{this.state.squadId ? 'Squad: ' + this.state.squadId : 'No Squad'}</p>
                        
                        <button className="btn" onClick={this.editClick}>Edit</button>
                        <button className="btn" onClick={() => {this.deletePlayer(game, player)}}>Delete</button>
                    </div>
                }
            </div>
        ) : (
            <div className="text-left">
                
                    <div>
                        {player.id + ': '}
                        {player.username}
                    </div>
                        <div>
                            <label className="mr-3">Is Human:</label>
                            <input className="mr-3" type="checkbox" name="isHuman" checked={this.state.isHuman} onChange={this.changeIsHuman} /><br/>
                            <label className="mr-3">Is Patient Zero</label>
                            <input className="mr-3" type="checkbox" name="isPatientZero" checked={this.state.isPatientZero} onChange={this.changeIsPatientZero} /><br/>
                            
                            {/* <label className="mr-3">Squad:</label>
                            <select value={this.state.player.squadId} onChange={e => {this.changeSquad(e)}}>
                                {squads}
                            </select> */}
                        
                            <button className="btn" onClick={() => this.handleSubmit(game, player)}>Submit Change</button>
                            <button className="btn btn-danger" onClick={this.editClick}>Cancel</button>
                        </div>
            </div>
        );
        return(
                <div className={styles.gdCard} onClick={this.toggleCard}>
                    {playerInfo}
                </div>
        );
    }
}
export default PlayerCard;