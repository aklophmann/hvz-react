import React from 'react';
import Styles from '../../../../views/Home/Home.module.css';

function SquadMember(props) {

    const isHuman = props.member.squadMember.isHuman ? <p>Human</p> : <p>Zombie</p>;
    const rank = props.member.squadMemberRank ? <p>Squad Leader</p> : <p>Rookie</p>

    return (
        <div className={Styles.Card}>
            {isHuman}
            {props.member.squadMember.username}
            {rank}
        </div>
    );
}
export default SquadMember;