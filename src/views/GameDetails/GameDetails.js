import React from 'react';

import GameTitle from '../../components/GameTitle/GameTitle';
import BiteCode from '../../components/BiteCode/BiteCode';
import JoinGame from '../../components/JoinGame/JoinGame';
import PlayerCard from '../../components/PlayerCard/PlayerCard';
import BiteCodeEntry from '../../components/BiteCodeEntry/BiteCodeEntry';
import SquadList from '../../components/SquadComponents/SquadList/SquadList';
import LeafletMap from '../../components/Map/LeafletMap';
import styles from '../Home/Home.module.css'
import SquadDetail from '../../components/SquadComponents/SquadDetail/SquadDetail';
import Kill from '../../components/Kill/Kill';
import Chat from '../../components/Chat/Chat';
import { isThursday } from 'date-fns';

class GameDetails extends React.Component {
    state = {
        game: this.props.location.game,
        joinedGame: false,
        user: JSON.parse(sessionStorage.getItem("user")),
        accessToken: sessionStorage.getItem("accessToken"),
        player: null,
    }
    componentWillMount = () => {
        if (this.state.user == null || (this.state.user.role != "ROLE_USER" && this.state.user.role != "ROLE_ADMIN")) {
            window.confirm("You have to log in to view this page\nDo you want to log in?") ? (
                this.props.history.push({
                    pathname: "../login",
                    state: { location: this.props.location.pathname }
                })) : (this.props.history.push("../"));
        }
    }
    componentDidMount = async () => {
        const game = this.state.game;
        console.log(game);
        if (!game) {
            let id = this.props.match.params.id;
            try {
                const resp = await fetch(`https://hvz-backend.herokuapp.com/game/${id}/`, {
                    Method: "GET",
                    headers: {
                        'Authorization': 'Bearer ' + this.state.accessToken
                    }
                }).then(resp => { return Promise.all([resp.status, resp.json()]) });

                console.log('Response: ' + resp);
                this.setState({
                    game: resp[1]
                });
                console.log("state game: " + this.state.game.id);

            } catch (e) {
                console.error(e);
            }
        }
        if (this.state.user && this.state.user.Players) {
            this.state.user.Players.forEach(player => {
                if (player.gameId == this.state.game.id) {
                    console.log(player);
                    this.setState({
                        joinedGame: true,
                        player: player,
                    });
                }
            });
        }

        this.interval = setInterval(() => {
             this.updateBoth();
         }, 5000); 
        console.log(this.state.game); 
    }

     componentWillUnmount() {
         clearInterval(this.interval);
     }

    joinGameClicked = async () => {
        let resp;
        try{
            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.game.id}/player`,{
                method:"POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken
                },
                body: JSON.stringify({
                    userId: this.state.user.id
                })
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });
            console.log(resp);
        }
        catch (e) { console.error(e) }

        // if request goes through and a player is returned add the player to the user in state and session
        if (resp && resp[0] == 200) {
            this.setState(state => {
                const user = state.user.Players.push(resp[1]);
                return user
            });
            sessionStorage.setItem("user", JSON.stringify(this.state.user));
            console.log(resp[1]);
            this.setState({
                joinedGame: true,
                player: resp[1]
            })
        }

    }

    editGameClicked = () => {
        this.props.history.push({
            pathname: '../newgame',
            title: `Edit Game ${this.state.game.id}`,
            game: this.state.game
        })
    }

    deletedPlayer = (id) => {
        let newGameState = JSON.parse(JSON.stringify(this.state.game));
        const playerToRemove = newGameState.playerList.map(player => { return player.id }).indexOf(id);
        ~playerToRemove && newGameState.playerList.splice(playerToRemove, 1);
        this.setState({ game: newGameState });
    }

    updateSquadID = (squadId) => {
        const player = this.state.player;
        player.squadId = squadId;

        const user = this.state.user;

        user.Players.forEach(player => {
            if (player.gameId == this.state.game.id) {
                player.squadId = squadId;
            }
        });

        sessionStorage.setItem("user", JSON.stringify(user));

        this.setState({
            player: player,
        })
        console.log(this.state.player);
    }

    updateBoth = () => {
        this.updateUser();
        this.updateGame();
        this.forceUpdate();
    }

    updateUser = async () => {
        let resp;
        try {
            resp = await fetch(`https://hvz-backend.herokuapp.com/game/users/${this.state.user.id}/`, {
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken
                }
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });
            console.log(resp);
        }
        catch (e) { console.error(e) }

        sessionStorage.setItem("user", JSON.stringify(resp[1]));
        resp[1].Players.forEach(player => {
            if (player.gameId == this.state.game.id) {
                console.log(player);
                this.setState({
                    user: resp[1],
                    player: player,
                });
            }
        });

    }

    updateGame = async () => {
        let resp;
        console.log("Update Game");
        try {
            resp = await fetch(`https://hvz-backend.herokuapp.com/game/${this.state.game.id}`, {
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.accessToken
                }
            }).then(resp => { return Promise.all([resp.status, resp.json()]) });
            console.log(resp);

            
        }
        catch (e) { console.error(e) }
        console.log(resp[1]);
        if(resp[0] === 200) {
            console.log("Sett new game state");
            console.log(resp[1]);
            this.setState({
                game: resp[1],
            });
        }


    }

    render() {
        let editGame;
        let joinGame;
        let biteCodeComponent;
        let squadList;
        let killList;
        let allKillList;

        if (this.state.user) {
            joinGame = this.state.joinedGame ? '' : <JoinGame game={this.state.game} onClick={this.joinGameClicked} />;
            editGame = this.state.user.role == 'ROLE_ADMIN' ? <button className="btn m-4" onClick={this.editGameClicked}>Edit Game</button> : '';
            biteCodeComponent = !this.state.joinedGame ? <div></div> : (this.state.player.isHuman ? <BiteCode biteCode={this.state.player.biteCode} /> : this.state.game.gameState === "In Progress" ? <BiteCodeEntry gameId={this.state.game.id} playerId={this.state.player.id} /> : <div></div>)
            squadList = !this.state.joinedGame || !this.state.player.isHuman ? <div></div> : (
                this.state.player.squadId == 0) ? (
                    <SquadList gameId={this.state.game.id} gameState={this.state.game.gameState} squadList={this.state.game.squadList} playerId={this.state.player.id} updateSquadID={this.updateSquadID} />) : (
                    <SquadDetail gameId={this.state.game.id} gameState={this.state.game.gameState} playerId={this.state.player.id} squadId={this.state.player.squadId} updateSquadID={this.updateSquadID} />)


            killList = this.state.player && this.state.player.killsList && this.state.player.killsList.length !== 0 ? this.state.player.killsList.map(kill => {
                return <Kill kill={kill} key={kill.id} />
            }) : <div>You have no kills</div>
            allKillList = this.state.game && this.state.game.killsList && this.state.game.killsList.length !== 0 ? this.state.game.killsList.map(kill => {
                let name = null;
                this.state.game.playerList.forEach(player => {
                    if (player.id === kill.killerId) {
                        name = player.username;
                    }
                })
                return <Kill kill={kill} name={name} key={kill.id} />
            }) : <div>No game kills</div>
        }

        if (this.state.game && this.state.game.hasOwnProperty('id')) {
            const players = this.state.game.playerList.map(player => {
                return <PlayerCard player={player} game={this.state.game} deletedPlayer={() => { this.deletedPlayer(player.id) }} key={player.id} />;
            });
            return (

                
                <div className="wrapper">
                    {this.state.player && 
                        <Chat player={this.state.player} game={this.state.game} />
                    }
                    <div className="baseDiv">
                        <div className={`${styles.Cards}`}>
                            <div className={`${styles.gdCard} gameTitleCard mb-4`}>
                                <GameTitle game={this.state.game} key={this.state.game.id} />
                                {editGame}
                                {this.state.game.gameState === "Registration" && joinGame}
                            </div>
                        </div>
                        <div className={`${styles.Cards} row`}>
                            <div className={`${styles.gdCard} col-md-4 mr-2`}>
                                {biteCodeComponent}
                            </div>
                            <div className={`${styles.gdCard} col-md-4 ml-2`}>
                                <h4>Squad</h4>
                                {squadList}
                            </div>
                        </div>
                        
                        {this.state.user && this.state.user.role == 'ROLE_ADMIN' && 
                            <div>
                                <div className={`${styles.Cards} row mt-4`}>
                                    <div className={`${styles.gdCard} col-md-4 mr-2`}>
                                        <h4>Registered Players</h4>
                                        {players}
                                    </div>
                                    <div className={`${styles.gdCard} col-md-4 ml-2`}>
                                        <h4>Game Kills</h4>
                                        {allKillList}
                                    </div>
                                </div>
                            </div>
                        }
                        {this.state.player && !this.state.player.isHuman &&
                            <div className={`${styles.Cards}`}>
                                <div className={`${styles.Card} gameTitleCard`}>
                                <h4>Your Kills</h4>
                                    {killList}
                                </div>
                            </div>
                        }
                        <LeafletMap game={this.state.game} player={this.state.player} />
                    </div>
                </div>

            );
        }
        else {
            return (<p>Loading...</p>);
        }
    }
}
export default GameDetails;