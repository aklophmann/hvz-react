import React from 'react';
import styles from '../../views/Home/Home.module.css';
import { HashLink as Link } from 'react-router-hash-link';

class GameCard extends React.Component{
    state = {
        clicked: false
    }

    clickedCard(){
        this.setState({
            clicked: !this.state.clicked
        });
    }

    gameDetailsClick = () => {
        this.props.history.push({
            pathname: `gamedetails/${this.props.card.id}`,
            game: this.props.card
        })
    }
    
    render(){
        
        const card = this.props.card;
            return (
                <div className={`${styles.gdCard} gameTitleCard`} onClick={this.clickedCard.bind(this)}>

                    <div>
                        <b>{card.name}</b>    
                    </div>
                    {this.state.clicked &&
                    <div>
                        <p>Date: {card.startDate}</p>
                        <p>{this.props.card.playerList.length} registered players</p>
                            <Link to={{
                                pathname: `gamedetails/${card.id}`,
                                game: card
                            }} className="btn">Game Details</Link>
                        
                        {/* <button className="btn">
                            <Link to={{
                                pathname: `gamedetails/${card.id}#register`, 
                                game: card
                            }}>Join Game</Link>
                        </button> */}
                    </div>
                    }
                </div>
            );
    }
}

export default GameCard;