
import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import Home from './views/Home/Home';
import Rules from './views/Rules/Rules';
import Nav from './components/Nav/Nav';
import Login from './components/Login/Login';
import LeafletMap from './components/Map/LeafletMap';
import GameDetails from './views/GameDetails/GameDetails';
import BiteCodeEntry from './components/BiteCodeEntry/BiteCodeEntry';
import CreateMissionModal from './components/Map/CreateMissionModal/CreateMissionModal';
import UpdateMissionModal from './components/Map/UpdateMission/UpdateMissionModal';
import CreateGame from './components/CreateGame/CreateGame';
import Chat from './components/Chat/Chat';
import SquadList from './components/SquadComponents/SquadList/SquadList'

function App() {
  let user = JSON.parse(sessionStorage.getItem('user'));
  return (
    <React.Fragment>
      <Router>
        <div className='wrapper'>
          <Nav />
          
          <div id='content' className='baseDiv'>
            <div className={user ? 'topBannerGreen' : 'topBannerRed'}>{user ? `Signed in as ${user.username}` : 'Not signed in, to get full use of this web-app, please sign in.'}</div>
            <h1 className="App pt-5">Human vs Zombies!</h1>
            <div>
              <Route exact path="/" component={Home} />
              <Route path="/rules" component={Rules} />
              <Route path="/login" component={Login} />
              <Route path="/gamemap" component={LeafletMap} />
              <Route path='/gamedetails/:id' component={GameDetails} />
              <Route path='/biteCodeEntry' component={BiteCodeEntry}/>
              <Route path='/updatemission' component={UpdateMissionModal}/>
              <Route path='/newgame' component={CreateGame}/>
              <Route path='/newmission' component={CreateMissionModal}/>
              <Route path='/squadList' component={SquadList} />
              <Route path='/chat' component={Chat} />
            </div>
          </div>
        </div>
      </Router>
    </React.Fragment>
  );
}

export default App;
