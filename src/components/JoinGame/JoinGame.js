import React from 'react';

const JoinGame = (props) => {
    return(
        <div>
            <button className="btn" onClick={props.onClick}>Join Game</button>
        </div>
    );

}
export default JoinGame;