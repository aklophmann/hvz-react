import React from 'react';
import { Map, TileLayer, Marker, Popup, CircleMarker, Circle } from 'react-leaflet';
import { LatLng } from 'leaflet';
import styles from '../../views/Home/Home.module.css'

class CreateGame extends React.Component {
    state = {
        user: JSON.parse(sessionStorage.getItem("user")),
        gameName: "",
        gameDate: "",
        gameDescription: "",
        gameLongitude: "",
        gameLatitude: "",
        mapLatLng: new LatLng(59.913691, 10.740133),
        formErrors: {
            gameName: '',
            gameState: '',
            gameDate: '',
            gameDescription: '',
            gameLatitude: '',
            gameLongitude: '',
            currentPos: null, 
        },
        formValid: false,
        nameValid: false,
        descriptionValid: false,
        dateValid: false,
        longitudeValid: false,
        latitudeValid: false
    }
    componentWillMount = () => {
        if (this.state.user == null || (this.state.user.role != "ROLE_USER" && this.state.user.role != "ROLE_ADMIN")) {
            this.props.history.push("../");
        }

    }

    componentDidMount = () => {
        if (this.props.location.game) {
            this.setState({
                gameName: this.props.location.game.name,
                gameDate: this.props.location.game.startDate,
                gameState: this.props.location.game.gameState,
                gameDescription: this.props.location.game.description,
                gameLongitude: this.props.location.game.longitude,
                gameLatitude: this.props.location.game.latitude,
                mapLatLng: new LatLng(this.props.location.game.latitude, this.props.location.game.longitude)
            });
            this.validateAll();
        }
    }

    validateInput = (fieldName, value) => {
        console.log('validate');
        let validationErrors = this.state.formErrors;
        let nameValid = this.state.nameValid;
        let descriptionValid = this.state.descriptionValid;
        let dateValid = this.state.dateValid;
        let longitudeValid = this.state.longitudeValid;
        let latitudeValid = this.state.latitudeValid;
        console.log('Field name: ' + fieldName + '\nValue: ' + value);
        switch (fieldName) {
            case 'name':
                nameValid = (/^[a-zA-Z0-9.,!#$%&'*+/=?^_`{|}~ -]*$/).test(value);
                validationErrors.gameName = nameValid ? '' : 'is invalid';
                break;
            case 'description':
                descriptionValid = (/^[a-zA-Z0-9.,!#$%&'*+/=?^_`{|}~ -]*$/).test(value);
                validationErrors.gameDescription = descriptionValid ? '' : 'is invalid';
                break;
            case 'date':
                dateValid = (/^[0-9.-]*$/).test(value);
                validationErrors.gameDate = dateValid ? '' : 'is invalid';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: validationErrors,
            nameValid: nameValid,
            descriptionValid: descriptionValid,
            formValid: this.state.nameValid && this.state.descriptionValid
        });
        console.log('form valid: ' + this.state.formValid);
        console.log('name valid: ' + this.state.nameValid);
        console.log('decription valid: ' + this.state.descriptionValid);
    }

    validateAll = () => {
        let validationErrors = this.state.formErrors;
        let nameValid = this.state.nameValid;
        let descriptionValid = this.state.descriptionValid;
        let dateValid = this.state.dateValid;
        let longitudeValid = this.state.longitudeValid;
        let latitudeValid = this.state.latitudeValid;

        nameValid = (/^[a-zA-Z0-9.,!#$%&'*+=?^_`|~ -]*$/).test(this.state.gameName);
        validationErrors.gameName = nameValid ? '' : 'is invalid';

        descriptionValid = (/^[a-zA-Z0-9.,!#$%&'*+=?^_`|~ -]*$/).test(this.state.gameDescription);
        validationErrors.gameDescription = descriptionValid ? '' : 'is invalid';

        dateValid = (/^[0-9.-]*$/).test(this.state.gameDate);
        validationErrors.gameDate = dateValid ? '' : 'is invalid';


        this.setState({
            formErrors: validationErrors,
            nameValid: nameValid,
            descriptionValid: descriptionValid,
            dateValid: dateValid,
            formValid: nameValid && descriptionValid && dateValid
        });
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        console.log('game state: ' + this.state.gameState);
        if (this.state.formValid) {
            let resp;
            try {
                const url = this.props.location.game ? (
                    `https://hvz-backend.herokuapp.com/game/${this.props.location.game.id}`
                ) : `https://hvz-backend.herokuapp.com/game/`;
                const method = this.props.location.game ? 'PUT' : 'POST';

                resp = await fetch(url, {
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + sessionStorage.getItem('accessToken')
                    },
                    body: JSON.stringify({
                        name: this.state.gameName,
                        startDate: this.state.gameDate,
                        gameState: this.state.gameState,
                        description: this.state.gameDescription,
                        latitude: this.state.gameLatitude,
                        longitude: this.state.gameLongitude
                    })
                }).then(resp => resp.json() /* {return Promise.all([resp.status, resp.json()])} */);

            } catch (e) {
                console.error(e);
            }
            console.log(resp);
            if (resp && this.props.location.game) {
                this.props.history.push(`../gamedetails/${this.props.location.game.id}`);
            }
            else if (resp) {
                this.props.history.push('../');
            }
        }

    }

    handleChange = (e) => {
        console.log('target name: ' + e.target.name);
        const name = e.target.name;
        const value = e.target.value;
        switch (e.target.name) {
            case 'name':
                this.setState({
                    gameName: e.target.value
                });
                this.validateInput(name, value);
                break;
            case 'state':
                this.setState({ gameState: e.target.value });
                console.log('game state: ' + this.state.gameState);
                break;
            case 'description':
                this.setState({
                    gameDescription: e.target.value
                });
                this.validateInput(name, value);
                break;
            case 'date':
                this.setState({
                    gameDate: e.target.value
                });
                this.validateInput(name, value);
                break;
            default:
                break;
        }

    }

    cancelClicked = () => {
        if (this.props.location.game) {
            this.props.history.push(`../gamedetails/${this.props.location.game.id}`);
        }
        else {
            this.props.history.push('../');
        }
    }

    handleMapClick = (e) => {
        //this.setState({ mapLatLng: null });
        this.setState({ 
            mapLatLng: e.latlng,
            gameLatitude: e.latlng.lat,
            gameLongitude: e.latlng.lng
        });
        console.log("map clicked at: " + e.latlng);
        console.log("lat" + this.state.gameLatitude);
        console.log("lng" + this.state.gameLongitude);
    }

    render() {
        const title = this.props.location.title ? this.props.location.title : "Create New Game";
        const disable = this.props.location.title ? false : true;

        const maptiles = 'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=qm2OZ0KQzznHTMyyAuet';
        const mapAttribution = '<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>';
       
        let zoomLevel = 12;
        return (
            <div>
                <h4>{title}</h4>
                <form onSubmit={this.handleSubmit}>
                    <div className={`${styles.Cards}`}>
                        <div className={`${styles.Card}`}>
                            <label>Game Title:</label><br />
                            <input type="text" name="name" placeholder="Enter game title" value={this.state.gameName} onChange={e => this.handleChange(e)} /><br />
                            <label>Status: </label><br />
                            <select name="state" disabled={disable} onChange={e => this.handleChange(e)}>
                                <option value="Registration">Registration</option>
                                <option value="In Progress">In Progress</option>
                                <option value="Complete">Complete</option>
                            </select><br />
                            <label>Start Date:</label><br />
                            <input type="date" name="date" value={this.state.gameDate} onChange={e => this.handleChange(e)} /><br />
                        </div>
                        <div className={`${styles.Card}`}>
                            <label>Description:</label><br />
                            <textarea name="description" rows="5" cols="50" placeholder="Enter description" value={this.state.gameDescription} onChange={e => this.handleChange(e)} /><br />
                        </div>
                        <div className={`${styles.Card}`}>
                            <label>Place:</label><br />
                            <Map ref='map' style={{ height: '300px' }} center={this.state.mapLatLng} zoom={zoomLevel} onClick={this.handleMapClick}>
                                <TileLayer
                                    url={maptiles}
                                    attribution={mapAttribution}
                                />
                                <Circle center={this.state.mapLatLng} radius={1000} color="red"></Circle>
                            </Map>
                            {/* <input type="number" name="latitude" placeholder="Enter latitude" value={this.state.gameLatitude} onChange={e => this.handleChange(e)} /><br /> */}
                            {/* <input type="number" className="mt-2" name="longitude" placeholder="Enter longitude" value={this.state.gameLongitude} onChange={e => this.handleChange(e)} /><br /> */}
                            <input type="submit" className="btn mt-4" disabled={!this.state.formValid} />
                            <button className="btn mt-4" onClick={this.cancelClicked}>Cancel</button>
                        </div>
                    </div>
                </form>

            </div>
        );
    }
}
export default CreateGame;